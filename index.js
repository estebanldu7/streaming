'use strict'

var
    express = require('express')(),
    http = require('http').createServer(express),
    io = require('socket.io')(http),
    port =  process.env.PORT || 3000,
    publicDir = __dirname + '/public';

http.listen(port, function () {
    console.log("Iniciando Express y Socket.IO en localhost: %d", port);
})

express
    .get('/', function (req, res) {
        res.sendFile(publicDir + '/client.html')
    })
    .get('/streaming', function (req, res) {
        res.sendFile(publicDir + '/server.html')
    });

io.on('connection', function (socket) {
    socket.on('streaming', function (image) {
        io.emit('play stream', image)
        console.log(image);
    })
})